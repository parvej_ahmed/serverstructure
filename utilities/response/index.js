const successResponse = (message,data) => {
    let response = {message:message,data:data};
    return {
        errorMessage: JSON.stringify(response)
    };
};

const badRequest = (message,data) => {
    let response = {message,data,statusCode:"[400]"};
    return JSON.stringify(response);
};

const internalServer = (message,data) => {
    let response = {message,data,statusCode:"[500]"};
    return JSON.stringify(response);
};

export {
    successResponse,
    badRequest,
    internalServer
};