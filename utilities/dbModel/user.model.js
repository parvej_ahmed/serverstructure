import mongoose, { Schema } from 'mongoose';

const userSchema = new Schema({
    email: { type: String },
    password: { type: String }
});

const User = mongoose.model('user', userSchema);

export {
    User
};