import JWT from "jsonwebtoken";
const key = "mycustomsecretkey";

const main = async(event) => {
    try{
        let token = event.authorizationToken;
        let decode = JWT.verify(token,key);
        return {
            principalId:decode.id,
            policyDocument:generatePolicy('Allow','execute-api:Invoke',event.methodArn)
        };
    }catch(err){
        return {
            principalId:"12345",
            policyDocument:generatePolicy('Deny','execute-api:Invoke',event.methodArn)
        };
    }
};

const generatePolicy = (effect,action,resource) => {
    let policy = {};
    policy.Version = "2012-10-17";
    policy.Statement = [];
    let statementObject = {};
    statementObject.Effect = effect;
    statementObject.Action = action;
    statementObject.Resource = resource;
    policy.Statement.push(statementObject);
    return policy;
};

export {
    main
};