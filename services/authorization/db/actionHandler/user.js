import { User } from '../../../../utilities/dbModel/user.model';

const UserCollectionActionHandler = async (event) => {
    console.log("user collection action handlelr");
    switch (event.actionType) {
        case 'registerUser': return RegisterUser(event);
        case 'loginUser': return loginUser(event);
    }
};

const findOne = async (event) => {
    try {
        let result = await User.findOne(event.selectionCriteria);
        return result;
    } catch (err) {
        throw ({ status: 'fail', message: "unable to find the user" });
    }
};

const insertDocument = async (event) => {
    try {
        let newDoc = new User(event.payload);
        let result = await newDoc.save();
        return result;
    } catch (err) {
        throw ({ status: 'fail', message: 'unable to save the user' });
    }
};

const RegisterUser = async (event) => {
    try {
        let user = await findOne(event);
        if (!user) {
            console.log("user is ", user);
            await insertDocument(event);
            return { status: 'success', message: "user created" };
        } else {
            throw ({ status: 'fail', message: 'user already exist' });
        }
    } catch (err) {
        throw (err);
    }
};

const loginUser = async (event) => {
    try {
        let result = await findOne(event);
        if (result) {
            if (result.password == event.payload.password) {
                return { status: 'success', message: 'successfully fetch the user', data: result._id };
            } else {
                throw ({ status: 'fail', message: '[400] please check your password' });
            }
        } else {
            throw ({ status: 'fail', message: '[400] please check your email id' });
        }
    } catch (err) {
        throw (err);
    }
};

export {
    UserCollectionActionHandler
};