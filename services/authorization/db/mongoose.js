/*eslint no-undef: "off"*/
const mongoose = require("mongoose");
let connection = null;

const makeDBConnection = async () => {
    try{
        if(!connection){
            connection = await mongoose.connect("mongodb+srv://sonu:parvez@cluster0-1nimi.mongodb.net/check?retryWrites=true&w=majority");
        }
        return "connection established";
    }catch(err){
        throw({type:"error in db connection",message:"there is an error while connecting to the db"});
    }
};

export {
    makeDBConnection
};

