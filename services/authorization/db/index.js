
import { makeDBConnection } from './mongoose';
import { UserCollectionActionHandler } from './actionHandler/user';

const main = async (event) => {
    try {
        await makeDBConnection();
        let result = await processEvent(event);
        console.log(result);
        return result;
    } catch (err) {
        return err;
    }
};

const processEvent = async (event) => {
    console.log("process event called");
    switch (event.collectionName) {
        case 'User': return UserCollectionActionHandler(event);
    }
};

export {
    main
};