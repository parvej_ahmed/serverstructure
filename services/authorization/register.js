/*eslint no-undef: "off"*/

import { AWS } from '../../utilities/sdk/aws-sdk';

const lambda = new AWS.Lambda();

const main = async (event) => {
  try {
    let functionInvocationParams = {
      FunctionName: 'authorizer-db-handler',
      Payload: JSON.stringify({ collectionName: 'User', actionType: 'registerUser', selectionCriteria: { email: event.email }, payload: event }),
    };
    let result = await lambda.invoke(functionInvocationParams).promise();
    console.log("env::::::::::::",process.env.MY_SECRET);
    return JSON.parse(result["Payload"]);
  } catch (err) {
    return err;
  }
};

export {
  main
};