/*eslint no-undef: "off"*/
import { AWS } from '../../utilities/sdk/aws-sdk';

const lambda  = new AWS.Lambda();
const jwt = require('jsonwebtoken');

const main = async (event) => {
    try{
        let functionInvocationParams = {
            FunctionName:'authorizer-db-handler',
            Payload:JSON.stringify({collectionName:'User',actionType:'loginUser',selectionCriteria:{email:event.email},payload:event}),
        };
        let result = await lambda.invoke(functionInvocationParams).promise();
        result = JSON.parse(result.Payload);
        if(result.status == "success"){
            let token = jwt.sign({id:result.data},"mycustomsecretkey",{expiresIn:'1h'});
            return {token:token};
        }else{
             throw(result.message);
        }
    }catch(err){
        throw(err);
    }
};

export {
    main
};